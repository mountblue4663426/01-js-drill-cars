export function problem2(inventory) {
  if (!Array.isArray(inventory)) {
    return "Passed Object is not array !";
  }

  let desiredCar = null;

  if (inventory.length > 0) {
    desiredCar = inventory[inventory.length - 1];
  }

  if (desiredCar) {
    return `Last car is a ${desiredCar.car_make} ${desiredCar.car_model}`;
  } else {
    return "There are no cars !";
  }
}
