export function problem1(inventory) {
  if (!Array.isArray(inventory)) {
    return "Passed Object is not array !";
  }

  let desiredCar = null;
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].id == 33) {
      desiredCar = inventory[index];
      break;
    }
  }

  if (desiredCar != null) {
    return `Car 33 is a ${desiredCar.car_year} ${desiredCar.car_make} ${desiredCar.car_model}`;
  } else {
    return "Car with year 33 does not exist !";
  }
}
