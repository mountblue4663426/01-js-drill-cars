import { inventory } from "../inventory.js";
import { problem3 } from "../problem3.js";

let resultArray = problem3(inventory);

if (Array.isArray(resultArray)) {
  for (let index = 0; index < resultArray.length; index++) {
    console.log(resultArray[index]);
  }
} else {
  console.log(resultArray);
}
