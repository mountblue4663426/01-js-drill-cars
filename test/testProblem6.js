import { inventory } from "../inventory.js";
import { problem6 } from "../problem6.js";

let result = problem6(inventory);

if (Array.isArray(result)) {
  console.log(JSON.stringify(result));
} else {
  console.log(result);
}
