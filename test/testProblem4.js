import { inventory } from "../inventory.js";
import { problem4 } from "../problem4.js";

let result = problem4(inventory);

if (Array.isArray(result)) {
  for (let index = 0; index < result.length; index++) {
    console.log(result[index]);
  }
} else {
  console.log(result);
}
