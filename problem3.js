export function problem3(inventory) {
  if (!Array.isArray(inventory)) {
    return "Passed Object is not array !";
  }

  inventory.sort((car1, car2) => {
    let model1 = car1.car_model.toUpperCase();
    let model2 = car2.car_model.toUpperCase();
    if (model1 === model2) {
      return 0;
    } else if (model1 > model2) {
      return 1;
    } else return -1;
  });

  return inventory;
}
