export function problem4(inventory) {
  if (!Array.isArray(inventory)) {
    return "Passed Object is not array !";
  }

  let carYears = [];

  for (let index = 0; index < inventory.length; index++) {
    carYears.push(inventory[index].car_year);
  }

  return carYears;
}
