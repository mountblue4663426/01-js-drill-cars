export function problem6(inventory) {
  if (!Array.isArray(inventory)) {
    return "Passed Object is not array !";
  }

  let BMWAndAudi = [];
  for (let index = 0; index < inventory.length; index++) {
    if (
      inventory[index].car_make === "BMW" ||
      inventory[index].car_make === "Audi"
    ) {
      BMWAndAudi.push(inventory[index]);
    }
  }

  return BMWAndAudi;
}
