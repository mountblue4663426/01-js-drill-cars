export function problem5(inventory, problem4) {
  let problem4Array = problem4(inventory);

  if (!Array.isArray(inventory)) {
    return "Passed Object is not array !";
  }

  let carOlderThan2k = [];

  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].car_year < 2000) {
      carOlderThan2k.push(inventory[index]);
    }
  }
  return carOlderThan2k;
}
